export const LOGIN = "login";
export const LOGOUT = "logout";
export const SHOW_ERROR = "showError";
export const HIDE_ERROR = "hideError";
export const CHECK_USER = "checkUser";