export const GET_BOOKING = "getBooking";
export const SELECT_SEAT = "selectSeat";
export const COUNTING_DOWN = "countDown";
export const STOP_COUNTING_DOWN = "stopCountingDown";
export const RESET_TIME = "resetTime";
export const NEXT_STEP = "nextStep";
export const PREV_STEP = "prevStep";
export const BOOKING = "booking";

