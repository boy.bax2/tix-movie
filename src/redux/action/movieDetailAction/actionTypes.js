export const FETCH_MOVIE_DETAIL = "fetchMovieDetail";
export const GET_MOVIE_TRAILER = "getMovieTrailer";
export const DROP_MOVIE_TRAILER = "dropMovieTrailer";
export const SWITCH_MOVIE_DETAIL_NAV = "switchMovieDetailNav";
