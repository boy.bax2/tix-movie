export const FETCH_MOVIE_LIST = "FETCH_MOVIE_LIST";
export const FETCH_THEATER_LIST = "FETCH_THEATER_LIST";
export const FETCH_THEATER_SHOWTIME = "FETCH_THEATER_SHOWTIME";
export const GET_HOURS_LIST = "GET_THEATER_HOURS_LIST";
export const START_LOADING = "START_LOADING";
export const STOP_LOADING = "STOP_LOADING";
export const ADD_NAME_THEATER = "ADD_NAME_THEATER";
export const ADD_NAME_MOVIE = "ADD_NAME_MOVIE";
export const ADD_NAME_DATE = "ADD_NAME_DATE";
export const ADD_NAME_HOURS = "ADD_NAME_HOURS";
export const GET_LIST_DATE = "GET_LIST_DATE";
export const REFRESH_FILM = "REFRESH_FILM";
export const REFRESH_THEATER = "REFRESH_THEATER";
export const REFRESH_DATE = "REFRESH_DATE";
